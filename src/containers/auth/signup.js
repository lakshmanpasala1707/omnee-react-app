import React, {Component} from 'react';
import classNames from 'classnames';
import {browserHistory} from 'react-router';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

class SignUp extends Component {
  constructor(props) {
        super(props);
        this.state = Object.assign({}, {elvalues: {
            name: '',
            email: '',
            password: '',
            confirmpassword: ''
        }});
  }

  render() {
    return (
      <div className="container">
        <div className="omb_login">

		<div className="row omb_row-sm-offset-3">
			<div className="col-xs-12 col-sm-6">	
			    <form id="form_change" className="omb_loginForm" onSubmit={this.handleSubmit.bind(this)}>
            <div className="input-group">
                <span className="input-group-addon"><i className="fa fa-user"></i></span>
                <input type="text" value={this.state.elvalues.name} onChange={this.handleChange.bind(this)} className="form-control login_input" name="name" placeholder="name"/>
            </div>
            <span className="help-block"></span>
        
            <div className="input-group">
                <span className="input-group-addon"><i className="fa fa-envelope"></i></span>
                <input type="text" value={this.state.elvalues.email} onChange={this.handleChange.bind(this)} className="form-control login_input" name="email" placeholder="email address"/>
            </div>
            <span className="help-block"></span>
                                
            <div className="input-group">
                <span className="input-group-addon"><i className="fa fa-lock"></i></span>
                <input  type="password" value={this.state.elvalues.password} onChange={this.handleChange.bind(this)} className="form-control login_input" name="password" placeholder="password"/>
            </div>
            <span className="help-block"></span>
            
            <div className="input-group">
                <span className="input-group-addon"><i className="fa fa-lock"></i></span>
                <input  type="password" value={this.state.elvalues.confirmpassword} onChange={this.handleChange.bind(this)} className="form-control login_input" name="confirmpassword" placeholder="confirm password"/>
            </div>
            <span className="help-block">{this.state.err}</span>
            <button className="btn btn-lg btn-sea btn-block" type="submit">Register</button>
            </form>
			</div>
    	</div>	    	
	</div>
    </div>) 
    }
  
    handleSubmit(values) {
       console.log(values);
       delete values['confirmpassword'];
       values['userType'] = this.state.userType;
    }
    
    handleChange() {
        
    }
  
}

function mapStateToProps(state) {

}

function matchDispatchToProps(dispatch){
}

export default connect(mapStateToProps, matchDispatchToProps)(SignUp);

React.defaultPorps = {
    isFullSection : false,
    activeUser: null
}