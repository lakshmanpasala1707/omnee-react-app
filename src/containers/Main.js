import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { calcDistances } from '../helpers/location';
import { Button } from 'react-mdl';
import { Link } from 'react-router';
import * as MainActions from '../reducers/main';
import {browserHistory} from 'react-router';

import LocationList from '../components/LocationList';
import AddButton from '../components/AddButton';

class Main extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div>
                <div className="main-left">
                    <img src="./images/home.png" style={{height: '100%', width: '100%', objectFit: 'contain'}}/>
                </div>
                <div className="main-right">
                    <p className="main-tag">Your one stop destination for all your hobby needs</p>
                    <button className="mdl-button mdl-js-button mdl-button--raised mdl-button--colored facebook-btn" data-upgraded=",MaterialButton">
                        CONTINUE WITH FACEBOOK
                    </button>
                    <div className="omb_hr">
                            <hr id="or_line" className="omb_hrOr"/>
                            <span  id="or_text" className="omb_spanOr">or</span>
                        </div>
                    <div>
                         <Link to="/signup">
                            <button className="mdl-button mdl-js-button mdl-button--raised sign-up">
                                <span className="login-text">SIGN UP</span>
                            </button>
                        </Link>
                        <button className="mdl-button mdl-js-button mdl-button--raised login">
                            <span className="login-text">LOGIN</span>
                        </button>
                    </div>
                </div>
            </div>
        );
    }
    
    onSignup() {
        browserHistory.push('/signup');
    }
}

export default  Main;
