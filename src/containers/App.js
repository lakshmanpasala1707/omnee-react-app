import 'react-mdl/extra/material';
import 'react-mdl/extra/material.css';

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { updateGeo } from '../reducers/main';
import { setSetting } from '../reducers/settings';
import { geoListeners } from '../helpers/setup';
import MainFooter from './Footer';
import { Navigation, Layout, Drawer, Content, Header, Switch } from 'react-mdl';

class App extends Component {
  componentWillMount() {
  }
  render() {
    return (
      <div class="main">
        <Layout fixedHeader>
          <Header className="headerStyle" title={
            <img src="./images/logo.png" style={{height: '60px', width: '150px'}}/>} >
            <Navigation style={{color: '#000000 !important'}}>
                <a href="#">How It Works</a>
                <a href="#">08:59</a>
            </Navigation>
            </Header>
            {this.props.children}
            <MainFooter/>
        </Layout>
    </div>);
  }
}

export default App;
