import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, compose, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import reducer from './reducers';
import App from './containers/App';
import persistState from './middleware/persistState';
import './style.css';
import {browserHistory, hashHistory, IndexRoute, Router, Route} from 'react-router';
import ReactMDL from 'react-mdl/out/ReactMDL';

import Main from './containers/Main';
import SignUp from './containers/auth/signup';

if (process.env.NODE_ENV === 'production' && navigator.serviceWorker) {
  navigator.serviceWorker.register('./sw.js')
  .then(() => console.log('Service worker registered'))
  .catch(err => console.log(`Service worker registration failed! ${err}`));
}

for(const component in ReactMDL) {
    if(ReactMDL.hasOwnProperty(component)) {
        window[component] = ReactMDL[component];
    }
}

const store = createStore(
    reducer,
);

ReactDOM.render(
  <Provider store={ store }>
      <Router history={browserHistory} >  
            <Route path='/' component={App}>
                <IndexRoute component={Main}/>
                <Route path='signup' component={SignUp}/>
            </Route>
      </Router>
  </Provider>,
  document.getElementById('react-root')
);
